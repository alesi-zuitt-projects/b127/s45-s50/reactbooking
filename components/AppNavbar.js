// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';

import {Navbar, Nav} from 'react-bootstrap';



export default function AppNavbar(){
	return(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand>Zuitt Booking System</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="#courses">Courses</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}

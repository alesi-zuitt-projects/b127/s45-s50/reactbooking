/*import {Fragment} from 'react'
import coursesData from "../data/courseData"
import CourseCard from '../components/CourseCard'

export default function Courses(){
	//check if courseData database was captured
	console.log(coursesData)
	console.log(coursesData[0])
	
	//for us to be able to display all the courses from the data file, we are going to use the map()
	// the ' map' metho dloops through the individual course objects in our array and returns a component for each course

	//multiple components created through the map method must have a unique key that will help react js identify  which / elements
	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp = {course} />
			)
	})
	return(
		<Fragment>
			<h1> Courses </h1>
			{courses}
		</Fragment>

		)

}*/

import {useState, useEffect, useContext} from 'react'

//bootstrap
import {Container} from 'react-bootstrap'
//components
import AdminView from '../components/AdminView'
import UserView from '../components/UserView'
//react context
import UserContext from '../UserContext'

export default function Courses() {

	const {user} = useContext(UserContext);

	const [allCourses, setAllCourses ] = useState([])

	const fetchData = () => {
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAllCourses(data)
		})
	}

	useEffect(() => {
		fetchData()
	},[])
	return(
		<Container>
			{
				(user.isAdmin === true) ?
				<AdminView coursesData={ allCourses } fetchData= {fetchData} />
				:
				<UserView coursesData={allCourses}/>




			}	
		</Container>
		)
}
import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Welcome from '../components/Welcome';


export default function Home() {
	return(
		<Fragment>
			< Welcome name="Jane" lName="Doe" />
			< Banner />
        	<Highlights />
        	
        </Fragment>
	)
}

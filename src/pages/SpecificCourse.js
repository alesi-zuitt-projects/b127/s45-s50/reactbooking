import { useState, useEffect, useContext} from 'react'
import { Container, Card, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Link, useHistory, useParams} from 'react-router-dom'

import UserContext from '../UserContext';

export default function SpecificCourse(){
    const {user} = useContext(UserContext)
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)

    const {courseId} = useParams()

    useEffect(() => {
        fetch(`http://localhost:4000/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
        })
    }, [])

    const enroll = () => {
        fetch('http://localhost:4000/users/enroll',{
            method: 'POST',
            headers:{
                'Content-Type': 'application/json',
                Authorization:`Bearer ${localStorage.getItem('accessToken') }`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true ){
                Swal.fire({
                    title: "You're now enrolled",
                    icon: "success",
                    text: `successfully enrolled to ${name}`
                })
            }else {
                Swal.fire({
                    title: "Failed to enroll",
                    icon: "error",
                    text: `failed enrolled to at ${name}`
                })
            }
        })
    }

    return(
        <Container>
            <Card className= "container-sm">
                <Card.Header className="bg-dark text-white text-center pb-0">
                    <h4>{name}</h4>
                </Card.Header>

                <Card.Body className = "text-center bg-warning" >
                    <Card.Text>
                        {description}
                    </Card.Text>
                    <h6>Php {price}</h6>
                </Card.Body>

                <Card.Footer className= "bg-danger">
                    {user.accessToken !== null ?
                    <Button variant="success" block onClick={() => enroll(courseId)}>Enroll</Button>
                    :
                    <Link className= "btn btn-danger btn-block" to="/login"> Login to enroll </Link>
                    }
                </Card.Footer>
            </Card>
        </Container>
    )
}
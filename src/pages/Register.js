import {Fragment, useEffect, useState, useContext, React} from 'react'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { Redirect, useHistory } from 'react-router-dom'



export default function Register(){
		const {user} = useContext(UserContext)
		const [firstName, setfirstName] = useState('');
		const [lastName, setlastName] = useState('');
		const [mobileNo, setmobileNo] = useState(''); 
		const [email, setEmail] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');
		const [isActive, setIsActive] = useState(false);
		const history = useHistory();
		console.log(firstName)
		console.log(lastName)
		console.log(mobileNo)
		console.log(email)
		console.log(password1)
		console.log(password2)


		useEffect(() => {
			if((firstName !== "" && lastName !== "" && email !== '' && password1 !=='' && password2 !=='') && (password1 === password2) && (mobileNo.length === 11)){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [firstName, lastName, mobileNo, email, password1, password2])

		function registerUser(e){
			e.preventDefault();

			fetch('http://localhost:4000/users/checkEmail', {
		        method: "POST",
		        headers: {
		        	'Content-Type': 'application/json'
		        },
		        body: JSON.stringify({
		        		email: email  		
        		})
   			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === true){
					Swal.fire({
						title: `${email}`,
						icon: "error",
						text:'Email already in use!!!'
					})
			
				}else{
					fetch('http://localhost:4000/users/register', {
				        method: "POST",
				        headers: {
			        	'Content-Type': 'application/json'
			       		},
						body: JSON.stringify({
			        		firstName: firstName,
			        		lastName: lastName,
			        		mobileNo: mobileNo,
			        		email: email,
			        		password: password1
	        				})
						})
						.then(res => res.json())
						.then(data => {
							Swal.fire({
							title: `${email}`,
							icon: 'success',
							text: 'Thank you for registering!!!'

							})
								history.push('/login')
						})
				
					}			
			
				})


			//to clear the data in the input fields
			setfirstName('')
			setlastName('')
			setmobileNo('')
			setEmail('')
			setPassword1('')
			setPassword2('')
			/*Swal.fire(
			  {
			  	title: 'Yayy!',
			  	icon: 'success',
			  	text: 'Successfully Registered'
			  }
				)*/
		
		}


		

		//Two way binding
		// the values in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is called two way binding
		//the data we changed in the view has updated the state
		//the data in the state has updated the view
	return(
		(user.accessToken !== null) ?
			<Redirect to = "/"/>
		:
	<Fragment>
		<h1> Register </h1>	
		<Form onSubmit = {(e) => registerUser(e)}> 
			<Form.Group>
				<Form.Label> First Name: </Form.Label>
				<Form.Control 
				type = "text" 
				placeholder = "Enter First Name"
				value= {firstName}
				onChange={e => setfirstName(e.target.value)}
				required
				/>
				
			</Form.Group>
			<Form.Group>
				<Form.Label> Last Name: </Form.Label>
				<Form.Control 
				type = "text" 
				placeholder = "Enter Last Name"
				value= {lastName}
				onChange={e => setlastName(e.target.value)}
				required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label> Contact Number: </Form.Label>
				<Form.Control 
				type='number'  
				placeholder = "Enter Contact No."
				value= {mobileNo}
				onChange={e => setmobileNo(e.target.value)}
				required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label> Email Address: </Form.Label>
				<Form.Control 
				type = "email" 
				placeholder = "Enter Email"
				value= {email}
				onChange={e => setEmail(e.target.value)}
				required
				/>
				<Form.Text className = "text-muted"> We'll never share your email with anyone else </Form.Text> 
			</Form.Group>
			<Form.Group>
				<Form.Label> Password: </Form.Label>
				<Form.Control 
				type = "password" 
				placeholder = "Enter Password"
				value= {password1}
				onChange={e => setPassword1(e.target.value)}
				required
				/>
				<Form.Text className = "text-muted"> We'll never share your password with anyone else </Form.Text> 
			</Form.Group>
			<Form.Group>
				<Form.Label> Verify Password: </Form.Label>
				<Form.Control 
				type = "password"

				placeholder = "Verify your Password"
				value= {password2}
				onChange={e => setPassword2(e.target.value)}
				required
				/>
				
			</Form.Group>
			{isActive ?
			<Button variant="primary" type="submit" id="submitBtn">Submit </Button>
			:
			<Button variant="primary" disabled type="submit" id="submitBtn">Submit </Button>
			}
		</Form>
		</Fragment>
		)
}
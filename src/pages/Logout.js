import {useContext, useEffect} from 'react'
import { Redirect } from 'react-router-dom'
import UserContext from '../UserContext'















export default function Logout() {

	const { setUser, unsetUser } = useContext(UserContext)
	unsetUser()

		useEffect(() => {
			setUser({ accessToken: null })
		}, [])
		//the array in our useEffect provide initial rendering only. once lang sya mag render
	return (
		//Redirect back to login
		<Redirect to = '/login' />
		)
}
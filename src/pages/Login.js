import {Fragment, useEffect, useState, useContext} from 'react'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
//routing import
import { Redirect, useHistory } from 'react-router-dom'

export default function Login(){
	//useHistory hook gives you access to the history instance that you may use to navigate
		const history = useHistory();
		//useContext is a react hook used to unwrap our context. It will return the data passed as values by a provider component in App.js
		const { user, setUser} = useContext(UserContext)
	
		const [email, setEmail] = useState('');
		const [password, setPassword] = useState('');
		const [isActive, setIsActive] = useState(false);
		console.log(email)
		console.log(password)

		useEffect(() => {
			if(email !== '' && password !==''){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [email, password])

		function loginUser(e){
			e.preventDefault();
			//fetch syntax 
			fetch('http://localhost:4000/users/loginUser', {
        method: "POST",
        headers: {
        	'Content-Type': 'application/json'
        },
        body: JSON.stringify({
        		email: email,
        		password: password
        	})
   		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken)
				setUser({ accessToken: data.accessToken })

				Swal.fire({
					title: 'yaaay!',
					icon: 'success',
					text: 'thank you for logging in!'
				})

				//get users details

				fetch('http://localhost:4000/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data.isAdmin === true){
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email: data.isAdmin,
							isAdmin: data.isAdmin
						})
						//ifAdmin redirect the page to /courses
						//push(path) pushes a new ebntry onto the history stack
						history.push('/courses')
					}else{
						//ifnot
						history.push('/')
					}
				})
			}else{
				/*Swal.fire({
					title: 'Oops!!!',
					icon: 'error',
					text: 'invalid credentials'
				})*/
				alert('please input a valid credentials')
			}
			setEmail('')
			setPassword('')
		})



			//to clear the data in the input fields
			/*Swal.fire(
			  {
			  	title: 'Yayy!',
			  	icon: 'success',
			  	text: 'Successfully Logged In!!!'
			  }
				)*/
			//allows us to save data within our browser as string
			// the setItem() method of the Storage interface, when passed a key name and value, will add that key to the given storage object, or update the key's value if its alreadt exists
			//setItem is used to store data in the localStorage as string
			//setItem('key', value)

			/*localStorage.setItem('email', email);
			setUser({email: email})
			setEmail('')
			setPassword('')
			*/
		}

		//create a condition rendering when a user is logged in



		//Two way binding
		// the values in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is called two way binding
		//the data we changed in the view has updated the state
		//the data in the state has updated the view
	return(
		(user.accessToken !== null) ?
			<Redirect to = "/"/>
		:
	<Fragment>
		<h1> Login </h1>	
		<Form onSubmit = {(e) => loginUser(e)}> 
			<Form.Group>
				<Form.Label> Email Address: </Form.Label>
				<Form.Control 
				type = "email" 
				placeholder = "Enter Email"
				value= {email}
				onChange={e => setEmail(e.target.value)}
				required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label> Password: </Form.Label>
				<Form.Control 
				type = "password" 
				placeholder = "Enter Password"
				value= {password}
				onChange={e => setPassword(e.target.value)}
				required
				/> 
			</Form.Group>
			{isActive ?
			<Button variant="primary" type="submit" id="submitBtn">Submit </Button>
			:
			<Button variant="primary" disabled type="submit" id="submitBtn">Submit </Button>
			}
		</Form>
		</Fragment>
		)
}
import {useState, useEffect, Fragment} from 'react'
import {Table, Button, Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'




export default function AdminView(props){	
	const {coursesData, fetchData} = props
	const [courses,setCourses] = useState([])
	//add state for addCourse
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	//state to modal add
	const [showAdd, setShowAdd] = useState(false)	
	//open a subject true or false kung mag show
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);
	//state for edit course
	const [showEdit, setShowEdit] = useState(false)
	//add state for courseId
	const[courseId, setCourseId] = useState('')
	// close and openedit for modals Update
	const closeEdit = () => {
		setShowEdit(false)
		setName('')
		setDescription('')
		setPrice(0)
	}
	const openEdit = (courseId) => {
		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			//populate all input values with the course information
			setCourseId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
		//then open modal
		setShowEdit(true)
	} 
	


	
	useEffect(() => {
	const coursesArray = coursesData.map(course => {
	return(
		<tr key={course.id}>
			<td>{course._id}</td>
			<td>{course.name}</td>
			<td>{course.description}</td>
			<td>{course.price}</td>
			<td className={course.isActive ? "text-success" : "text-danger"}>{course.isActive ? "Available" : "Unavailable"}</td>
			<td>
				<Button variant = "primary" size= "sm" onClick={ () => openEdit(course._id)}> Update </Button>
			</td>
			<td>
			{course.isActive ?
				<Button variant = "danger" size = "sm" onClick={	()	=> archiveToggle(course._id, course.isActive)	}> Disable </Button> : <Button variant = "success" size = "sm" onClick={	()	=> activateToggle(course._id, course.isActive)	}> Enable </Button>
			}				
			</td>
		</tr>
		)
	})
	setCourses(coursesArray)
}, [coursesData])

	const addCourse = (e) => {
		e.preventDefault();
		fetch('http://localhost:4000/courses/',{
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				//run our fecthdata function that we passed from our parent component in order to re render our page
				fetchData()
				Swal.fire({
					title: 'Successfully Added!',
					icon: 'success',
					text: 'Course will be viewable by students'
				})
				setName('')
				setDescription('')
				setPrice(0)
				// autoclose modals
				closeAdd()
				
			}else{
				Swal.fire({
					title: "Can't add course",
					icon: "error",
					text: "make sure valid inputs"
				})

			}
		})
	}
	// Edit course function
	const editCourse = (e, courseId) => {
		e.preventDefault()
		fetch(`http://localhost:4000/courses/${courseId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("accessToken")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Successfully Updated!',
					icon: 'success',
					text: 'Course is updated!!!'
				})
				closeEdit()
			}else{
				fetchData()
				Swal.fire({
					title: "Can't update course",
					icon: "error",
					text: "make sure valid inputs"
				})
			}
		})
	}
	
	//Archive a course
	const archiveToggle = (courseId, isActive) => {
		fetch(`http://localhost:4000/courses/${courseId}/archive`,{
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem("accessToken")}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title:"Successfully archived!!!",
					icon: "success",
					text:"course successfully disabled"
				})
			}else{
				fetchData()
				Swal.fire({
					title:"Error!!!",
					icon: "error",
					text: "archive error!!!"
				})
			}
		})
	}
	//Activate a course
	const activateToggle = (courseId, isActive) => {
		fetch(`http://localhost:4000/courses/${courseId}/activate`,{
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem("accessToken")}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title:"Successfully activated!!!",
					icon: "success",
					text:"course successfully activated"
				})
			}else{
				fetchData()
				Swal.fire({
					title:"Error!!!",
					icon: "error",
					text: "activate error!!!"
				})
			}
		})
	}
	return(
		<Fragment>
		<div className="text-center my-4"> 
			<h2>Admin Dashboard</h2>
			<div className= "d-flex justify-content-center">
			<Button variant="primary" onClick = {openAdd}> Add Course </Button>
			</div>
		</div>
		<Table striped bordered hover responsive>
			<thead className="bg-dark text-white">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Availability</th>
					<th>Actions</th>
					<th>Enable/Disable</th>
				</tr>


			</thead>
			<tbody>
			{courses}

			</tbody>

		</Table>

		<Modal show = {showAdd} onHide= {closeAdd}>
			<Form onSubmit = {e => addCourse(e)}>
				<Modal.Header>
					<Modal.Title>
						Add Course
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
						<Form.Group>
							<Form. Label>Name:</Form. Label>
							<Form.Control type = "text" value = {name} onChange= {e => setName(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Description:</Form. Label>
							<Form.Control type = "text" value = {description} onChange= {e => setDescription(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form. Label>Price:</Form. Label>
							<Form.Control type = "number" value = {price} onChange= {e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
					<Button variant="secondary" onClick = {closeAdd}> close </Button>
					<Button variant="success" type = "submit"> Add Course </Button>
					</Modal.Footer>
			</Form>
		</Modal>
		
		<Modal show = {showEdit} onHide= {closeEdit}>
			<Form onSubmit = {e => editCourse(e, courseId)}>
				<Modal.Header closeButton>
					<Modal.Title>
						Update Course
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
						<Form.Group>
							<Form.Label>Name:</Form.Label>
							<Form.Control type = "text" value = {name} onChange= {e => setName(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form.Label>Description:</Form.Label>
							<Form.Control type = "text" value = {description} onChange= {e => setDescription(e.target.value)} required />
						</Form.Group>
						<Form.Group>
							<Form.Label>Price:</Form.Label>
							<Form.Control type = "number" value = {price} onChange= {e => setPrice(e.target.value)} required />
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick = {closeEdit}> Close </Button>
						<Button variant="success" type="submit"> Submit </Button>
					</Modal.Footer>
			</Form>
		</Modal>
		</Fragment>
		)
}
import { useState, useEffect, Fragment } from 'react'
import { Button } from 'react-bootstrap'

//syntax of useEffect
/*
useEffect(() => {}, [])


*/
export default function Counter() {
	const [count, setCount] = useState(0)

	useEffect(()=>{
		document.title = `You clicked ${count} times`
	},[count])
	return (
		<Fragment>
			<p> You Clicked {count} </p>
			<button variant="primary" onClick={()=>setCount(count+1)}>Click here</button>
		</Fragment>
		)
}